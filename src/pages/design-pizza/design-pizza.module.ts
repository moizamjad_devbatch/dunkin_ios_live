import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DesignPizzaPage } from './design-pizza';

@NgModule({
  declarations: [
    DesignPizzaPage,
  ],
  imports: [
    IonicPageModule.forChild(DesignPizzaPage),
  ],
})
export class DesignPizzaPageModule {}
